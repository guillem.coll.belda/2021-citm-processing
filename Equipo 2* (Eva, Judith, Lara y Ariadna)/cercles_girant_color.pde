ArrayList<Bola> bolas = new ArrayList<Bola>();

void setup() {
  fullScreen();
}


void draw() {
  background(0);
  bolas.add(new Bola(50, 200, 1));
  bolas.add(new Bola(60, 200, 2));
  bolas.add(new Bola(70, 200, 3));
  bolas.add(new Bola(80, 200, 4));
  bolas.add(new Bola(90, 200, 5));
  bolas.add(new Bola(100, 200, 6));
  bolas.get(0).move();
  bolas.get(1).move();
  bolas.get(2).move();
  bolas.get(3).move();
  bolas.get(4).move();
  bolas.get(5).move();
  
  saveFrame("mates###.jpg");
}
